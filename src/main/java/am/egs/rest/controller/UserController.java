package am.egs.rest.controller;


import am.egs.rest.model.User;
import am.egs.rest.service.UserService;
import am.egs.rest.util.exception.AccessDeniedException;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @PutMapping("/registration")
    public ResponseEntity register(@RequestBody @Valid User user) {
        userService.register(user);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestParam String email,
                                @RequestParam String password) throws AccessDeniedException, NotFoundException {

        User user = userService.getUser(email);
        if (user == null) {
            throw new NotFoundException("please registration");
        }
        if (!password.equals(user.getPassword())) {
            throw new AccessDeniedException("wrong password");
        }

        return ResponseEntity.ok(user);
    }

}
