package am.egs.rest.service;

import am.egs.rest.model.User;

public interface UserService {
    void register(User user);

    User getUser(String email);

}
