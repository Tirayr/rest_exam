package am.egs.rest.service.impl;

import am.egs.rest.model.User;
import am.egs.rest.repository.UserRepository;
import am.egs.rest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public void register(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUser(String email) {
        return userRepository.getByEmail(email);
    }
}
