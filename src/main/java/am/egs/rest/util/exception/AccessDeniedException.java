package am.egs.rest.util.exception;

public class AccessDeniedException extends Exception{

    public AccessDeniedException (String message){
        super(message);
    }
}
